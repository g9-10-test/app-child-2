package ai.ecma.appchild2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppChild2Application {

    public static void main(String[] args) {
        SpringApplication.run(AppChild2Application.class, args);
    }

}
